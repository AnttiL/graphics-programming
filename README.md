# Graphics Programming

## Description

OpenGL Assignment for the Graphics Programming course.

## Build Requirements

* Linux
* X11
* C++17
* CMake v3.13+
* OpenGL v4.6+
* OpenGL Mathematics v0.9.9+
* GLFW
* spdlog

## How To Build

From the project root directory:

```zsh
mkdir build
cd build
cmake ..
make
```

## Running The Program

From the build/ directory:

```zsh
./bin/rpg-login-screen
```
## Demo

[Demo](https://i.imgur.com/541Dtk7.mp4)
