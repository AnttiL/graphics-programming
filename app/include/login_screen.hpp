// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <spdlog/spdlog.h>

#include "application.hpp"
#include "engine/game_object.hpp"
#include "engine/shader.hpp"
#include "engine/texture.hpp"
#include "engine/vertex.hpp"

namespace k1526 {
namespace implementation {

/// create an application
///
class login_screen
	: public k1526::application
{
public:
        /// constructor
        ///
        login_screen();
        /// destructor
        ///
	virtual ~login_screen();
private:
	/// handle on create event
	///
	auto on_create() -> void;
	/// handle on destroy event
	///
	auto on_destroy() -> void;
	/// handle on draw event
	///
	auto on_draw() -> void;
	/// handle on update event
	///
	auto on_update() -> void;

	GLuint program;

	std::vector<std::shared_ptr<k1526::engine::game_object>> game_objects;	///< collection of game objects
	std::vector<std::shared_ptr<k1526::engine::geometry>> geometries;	///< collection of geometries
	std::vector<std::shared_ptr<k1526::engine::shader>> shaders;
	std::vector<k1526::engine::texture> textures;

	k1526::engine::material material;
};

} // namespace implementation

// lift the implementation
using login_screen = implementation::login_screen;

} // namespace k1526
