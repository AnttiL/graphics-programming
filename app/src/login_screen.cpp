// Original work Jani Immonen
// Modified work Antti Lohtaja

#include "login_screen.hpp"

namespace k1526::implementation {

login_screen::login_screen()
	: game_objects{}
	, geometries{}
	, shaders{}
	, material{}
	, textures{}
{ }

login_screen::~login_screen()
{ }

auto login_screen::on_create() -> void
{
	// create textures
	textures.emplace_back(k1526::engine::texture{"textures/hiilikuitu.jpg"});
	textures.emplace_back(k1526::engine::texture{"textures/kuitua.jpg"});
	textures.emplace_back(k1526::engine::texture{"textures/lastuja.jpg"});
	textures.emplace_back(k1526::engine::texture{"textures/ledispektri.jpg"});

	// create shaders
	auto simple_position_shader = std::make_shared<k1526::engine::shader>("shaders/simple_position.vs", "shaders/simple_position.fs");
	auto simple_texture_shader = std::make_shared<k1526::engine::shader>("shaders/simple_texture.vs", "shaders/simple_texture.fs");
	auto position_and_texture_shader = std::make_shared<k1526::engine::shader>("shaders/position_and_texture.vs", "shaders/position_and_texture.fs");
	auto four_textures_shader = std::make_shared<k1526::engine::shader>("shaders/four_textures.vs", "shaders/four_textures.fs");
	shaders.emplace_back(simple_position_shader);
	shaders.emplace_back(simple_texture_shader);
	shaders.emplace_back(position_and_texture_shader);
	shaders.emplace_back(four_textures_shader);

	simple_texture_shader->activate();
	simple_texture_shader->set_int("texture01", 0);

	position_and_texture_shader->activate();
	position_and_texture_shader->set_int("texture01", 0);
	position_and_texture_shader->set_int("texture02", 1);

	four_textures_shader->activate();
	four_textures_shader->set_int("texture01", 0);
	four_textures_shader->set_int("texture02", 1);
	four_textures_shader->set_int("texture03", 2);
	four_textures_shader->set_int("texture04", 3);

	// create geometries
	auto cube_geometry = std::make_shared<k1526::engine::geometry>();
	auto plane_geometry = std::make_shared<k1526::engine::geometry>();
	auto sphere_geometry = std::make_shared<k1526::engine::geometry>();
	geometries.emplace_back(cube_geometry);
	geometries.emplace_back(plane_geometry);
	geometries.emplace_back(sphere_geometry);

	cube_geometry->generate_cube(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	plane_geometry->generate_plane(glm::vec3(1.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	sphere_geometry->generate_sphere(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), 24, 24);

	// setup our view and projection matrices
	auto view = std::make_shared<glm::mat4>(1.0f);
	auto projection = std::make_shared<glm::mat4>(1.0f);

	//*view = glm::translate(*view, glm::vec3{0.0f, 0.0f, -3.0f});
	//*projection = glm::perspective(glm::radians(45.0f), get_aspect_ratio(), 0.1f, 100.0f);

	// spawn a plane
	auto plane = std::make_shared<k1526::engine::game_object>();
	plane->set_view(view);
	plane->set_projection(projection);
	plane->attach_shader(shaders.at(3));
	plane->set_geometry(plane_geometry);
	plane->set_position(0.0f, 0.0f, 0.0f);
	game_objects.push_back(plane);

	// spawn a cube
	auto cube = std::make_shared<k1526::engine::game_object>();
	cube->set_view(view);
	cube->set_projection(projection);
	cube->attach_shader(shaders.at(3));
	cube->set_geometry(cube_geometry);
	cube->set_position(0.0f, 0.0f, 0.0f);
	//game_objects.push_back(cube);

	// spawn a sphere
	auto sphere = std::make_shared<k1526::engine::game_object>();
	sphere->set_view(view);
	sphere->set_projection(projection);
	sphere->attach_shader(shaders.at(2));
	sphere->set_geometry(sphere_geometry);
	sphere->set_position(0.0f, 0.0f, 0.0f);
	//game_objects.push_back(sphere);

	for (auto game_object : game_objects) {
		game_object->set_gravity(0.0f);
		game_object->set_rotation_angle(glm::linearRand(0.0f, 6.0f));
		game_object->set_rotation_speed(glm::linearRand(-1.0f, 1.0f));
	}
}

auto login_screen::on_destroy() -> void
{
	spdlog::debug("on destroy");
}

auto login_screen::on_draw() -> void
{
	// clear color buffer
	glClearColor(0.0f, 0.5f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // bind textures on corresponding texture units
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textures.at(0).get());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textures.at(1).get());
	glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textures.at(2).get());
	glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textures.at(3).get());

	for (auto game_object : game_objects) {
		game_object->draw();
	}

	return;

	// // setup the light position
	// const auto light_position = get_renderer()->get_light_position();
	// auto location = glGetUniformLocation(program, "lightPosition");
	// glUniform3f(location, light_position.x, light_position.y, light_position.z);

	// // setup the camera position
	// auto camera_position = glm::vec3{-get_renderer()->get_view_matrix()[3]};
	// location = glGetUniformLocation(program, "cameraPosition");
	// glUniform3f(location, camera_position.x, camera_position.y, camera_position.z);

	// // set the uv2 offset uniform
	// location = glGetUniformLocation(program, "uvOffset");
	// glUniform1f(location, uv_offset);

	// //glDisable(GL_BLEND);

	// auto i = int{0};
	// for (auto game_object : game_objects) {
	// 	// setup the texture for rendering
	// 	glActiveTexture(GL_TEXTURE0);
	// 	glBindTexture(GL_TEXTURE_2D, textures[0]);
	// 	location = glGetUniformLocation(program, "texture01");
	// 	glUniform1i(location, 0);
	// 	set_texturing_params();

	// 	if (i == 0)
	// 	{
	// 		glActiveTexture(GL_TEXTURE1);
	// 		glBindTexture(GL_TEXTURE_2D, textures[1]);
	// 		location = glGetUniformLocation(program, "texture02");
	// 		glUniform1i(location, 1);
	// 		set_texturing_params();
	// 	}
	// 	else
	// 	{
	// 		glActiveTexture(GL_TEXTURE1);
	// 		glBindTexture(GL_TEXTURE_2D, textures[2]);
	// 		location = glGetUniformLocation(program, "texture02");
	// 		glUniform1i(location, 1);
	// 		set_texturing_params();
	// 	}

	// 	glActiveTexture(GL_TEXTURE2);
	// 	glBindTexture(GL_TEXTURE_2D, textures[3]);
	// 	location = glGetUniformLocation(program, "texture03");
	// 	glUniform1i(location, 2);
	// 	set_texturing_params();

	// 	game_object->draw(get_renderer(), program);

	// 	/**
	// 	 * Your turn to code:
	// 	 * - Add 3rd texture to program, make it a grayscale version of earth texture
	// 	 * - Apply the 3rd texture in a shader as specular map
	// 	 * - Specular map determines which parts of the object are shine and which are not
	// 	 *
	// 	 */
	// 	++i;
	// }
}

auto login_screen::on_update() -> void
{
	for (auto game_object : game_objects)
		game_object->update();
}

} // namespace k1526::implementation
