// Antti Lohtaja
// K1526
// TTV15S4

#include <stdlib.h>
#include <stdio.h>
#include <string_view>

#include "linmath.h"
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "login_screen.hpp"

int main(void)
{
	spdlog::set_level(spdlog::level::debug); // Set global log level to debug

	auto app = k1526::login_screen{};

	auto window_title = std::string_view{"K1526's App"};
	constexpr auto window_width = int{1280};
	constexpr auto window_height = int{720};

	// initialize app
	if (not app.create(window_width, window_height, window_title)) {
		spdlog::error("Couldn't create app.");
		return EXIT_FAILURE;
	}

	// main run loop
	app.run();

	exit(EXIT_SUCCESS);
}
