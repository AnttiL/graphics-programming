#version 460 core
out vec4 out_color;

in vec2 texture_coordinates;

uniform sampler2D texture01;
uniform sampler2D texture02;
uniform sampler2D texture03;
uniform sampler2D texture04;

void main()
{
        // combine 4 textures
        if (texture_coordinates.y > 0.5) {
                if (texture_coordinates.x > 0.5) {
                        out_color = texture(texture01, texture_coordinates);
                } else {
                        out_color = texture(texture02, texture_coordinates);
                }
        } else {
                if (texture_coordinates.x > 0.5) {
                        out_color = texture(texture03, texture_coordinates);
                } else {
                        out_color = texture(texture04, texture_coordinates);
                }
        }
}
