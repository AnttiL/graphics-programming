#version 460 core
layout (location = 0) in vec3 our_position;
layout (location = 1) in vec3 our_normal;
layout (location = 2) in vec2 our_texture_coordinates;

out vec2 texture_coordinates;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
        gl_Position = model * view * projection * vec4(our_position, 1.0);
        texture_coordinates = our_texture_coordinates;
}
