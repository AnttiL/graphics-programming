#version 460 core
out vec4 out_color;

in vec2 texture_coordinates;

uniform sampler2D texture01;
uniform sampler2D texture02;

void main()
{
        // linearly interpolate between two textures
        out_color = mix(texture(texture01, texture_coordinates), texture(texture02, texture_coordinates), 0.2);
}
