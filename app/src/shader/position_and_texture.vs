#version 460 core
layout (location = 0) in vec3 our_position;
layout (location = 1) in vec2 our_texture_coordinates;

out vec2 texture_coordinates;

uniform mat4 transform;

void main()
{
        gl_Position = transform * vec4(our_position, 1.0);
        texture_coordinates = our_texture_coordinates;
}
