#version 460 core
out vec4 out_color;

in vec2 texture_coordinates;

uniform sampler2D texture01;

void main()
{
        out_color = texture(texture01, texture_coordinates);
}
