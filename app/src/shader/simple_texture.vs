#version 460 core
layout (location = 0) in vec3 our_position;
layout (location = 2) in vec2 our_texture_coordinates;

out vec2 texture_coordinates;

void main()
{
        gl_Position = vec4(our_position, 1.0);
        texture_coordinates = our_texture_coordinates;
}
