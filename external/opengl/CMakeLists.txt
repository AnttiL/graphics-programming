# ./external/opengl/CMakeLists.txt

# OpenGL version
set(OPENGL_VERSION 4.6)

# Find OpenGL
find_package(
        OpenGL ${OPENGL_VERSION}
        REQUIRED
)

# Make the OpenGL target globally available.
if (OpenGL_FOUND)
        set_target_properties(
                OpenGL::GL
                PROPERTIES
                        IMPORTED_GLOBAL TRUE
        )
endif()
