# ./external/x11/CMakeLists.txt

# Find X11
find_package(
        X11
        REQUIRED
)

# Make the X11 target globally available.
if (X11_FOUND)
        set_target_properties(
                X11::X11
                PROPERTIES
                        IMPORTED_GLOBAL TRUE
        )
endif()
