// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include <optional>
#include <string>

#include "linmath.h"
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <spdlog/spdlog.h>

namespace k1526 {
namespace implementation {

/// create an application
///
class application
{
public:
        using application_ptr = application*;
	using window_ptr = GLFWwindow*;
        /// constructor
        ///
        application();
        /// destructor
        ///
	virtual ~application();
	/// get window aspect ratio
	///
	auto get_aspect_ratio() const -> float;
	/// get window handle
	///
	auto get_window() -> window_ptr;
	/// handle window resize
	///
	static auto handle_resize(window_ptr window, int width, int height) -> void;
	/// handle an error
	///
	static auto handle_error(int error, const char* description) -> void;
	/// initialize the application layer
        ///
	/// \param x is the horizontal resolution of the window in pixels
	/// \param y is the vertical resolution of the window in pixels
	/// \param title is the window title
	/// \return true if successful, false otherwise
	auto create(int window_width, int window_height, std::string_view window_title) -> bool;
	/// enter into app main loop
	///
	auto run() -> void;
protected:
	/// handle on create event
	///
	virtual auto on_create() -> void;
	/// handle on draw event
	///
	virtual auto on_draw() -> void;
	/// handle on update event
	///
	virtual auto on_update() -> void;
private:
	/// create a window
	///
	auto create_window(int window_width, int window_height, std::string_view window_title) -> bool;

        std::shared_ptr<spdlog::logger> logger;	///< spdlog logger
	window_ptr window;
	bool active;                                    ///< application active status
	int32_t screen_width;                           ///< screen width
	int32_t screen_height;                         	///< screen height
};

} // namespace implementation

// lift the implementation
using application = implementation::application;

} // namespace k1526
