// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include <memory>

#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "geometry.hpp"
#include "material.hpp"
#include "shader.hpp"
#include "vertex.hpp"

namespace k1526::engine {
namespace implementation {

class game_object
{
public:
        using geometry_ptr = std::shared_ptr<k1526::engine::geometry>;
        using material_ptr = std::shared_ptr<k1526::engine::material>;
	using shader_ptr = std::shared_ptr<k1526::engine::shader>;
	using mat4_ptr = std::shared_ptr<glm::mat4>;
	/// constructor
	///
	game_object();
	/// destructor
	///
	virtual ~game_object();
	/// get geometry
	///
	auto get_geometry() const -> geometry_ptr;
	/// get gravity
	///
	auto get_gravity() const -> float;
	/// get material
	///
	auto get_material() -> material_ptr;
	/// get model matrix
	///
	auto get_matrix() -> glm::mat4&;
	/// get position
	///
	auto get_position() const -> glm::vec3;
	/// get radius
	///
	auto get_radius() const -> float;
	/// get rotation angle
	///
	auto get_rotation_angle() const -> float;
	/// get rotation speed
	///
	auto get_rotation_speed() const -> float;
	/// get velocity
	///
	auto get_velocity() -> glm::vec3&;
	/// attach a shader
	///
	auto attach_shader(shader_ptr shader_) -> void;
	/// set geometry
	///
	auto set_geometry(geometry_ptr geometry_) -> void;
	/// set gravity
	///
	auto set_gravity(float gravity_) -> void;
	/// set material
	///
	auto set_material(material_ptr material_) -> void;
	/// set game object position
	///
	auto set_position(float x, float y, float z) -> void;
	/// set game object position
	///
	auto set_position(const glm::vec3& position) -> void;
	/// set projection matrix
	///
	auto set_projection(mat4_ptr projection) -> void;
	/// set velocity
	///
	auto set_velocity(const glm::vec3& velocity_) -> void;
	/// set view matrix
	///
	auto set_view(mat4_ptr view) -> void;
	/// set rotation speed
	///
	auto set_rotation_speed(float speed) -> void;
	/// set rotation angle
	///
	auto set_rotation_angle(float angle) -> void;
	/// draw the game object
	///
	virtual auto draw() -> void;
	/// update the game object
	///
	virtual auto update() -> void;
protected:
	glm::mat4 model;
	mat4_ptr view;
	mat4_ptr projection;
	geometry_ptr geometry;
	material_ptr material;
	shader_ptr shader;

	// worlds simplest bounce code
	float rotation_angle;
	float rotation_speed;
	float gravity;
	glm::vec3 velocity;
};

} // namespace implementation

// lift the implementation
using game_object = implementation::game_object;

} // namespace k1526::engine
