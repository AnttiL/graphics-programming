// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include <array>
#include <cstdint>
#include <vector>
#include <iostream>
#include <memory>

#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <spdlog/spdlog.h>

#include "vertex.hpp"

namespace k1526::engine {
namespace implementation {

class geometry
{
public:
	using vertex = k1526::engine::vertex;
	/// constructor
	///
	geometry();
	/// destructor
	///
	virtual ~geometry();
	/// clear
	///
	auto clear() -> void;
	/// generate cube geometry
	///
	auto generate_cube(const glm::vec3& size, const glm::vec3& offset) -> void;
	/// generate a 2d plane
	///
	auto generate_plane(const glm::vec3& size, const glm::vec3& offset) -> void;
	/// generate sphere geometry
	///
	auto generate_sphere(const glm::vec3& radius, const glm::vec3& offset, uint32_t rings, uint32_t segments) -> void;

	auto set_attributes(GLuint program) const -> void;

	auto disable_attributes(GLuint program) const -> void;

	auto draw() const -> void;
	/// generate cube geometry
	///
	static auto generate_cube(const glm::vec3& size, const glm::vec3& offset, std::vector<vertex>& vertices, size_t& index_count) -> void;
	/// generate 2d plane geometry
	///
	static auto generate_plane(const glm::vec3& size, const glm::vec3& offset, std::vector<vertex>& vertices, size_t& index_count) -> void;
	/// generate sphere geometry
	///
	static auto generate_sphere(const glm::vec3& radius, const glm::vec3& offset, uint32_t rings, uint32_t segments, std::vector<vertex>& vertices) -> void;

	auto get_data() -> vertex*;
	auto get_data() const -> const vertex*;
	auto get_vertex_count() const -> size_t;
	auto get_index_count() const -> size_t;

private:
	std::vector<vertex> vertices;
	size_t index_count;
};

} // namespace implementation

// lift the implementation
using geometry = implementation::geometry;

} // namespace k1526::engine
