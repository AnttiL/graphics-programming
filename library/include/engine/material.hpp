// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include "glad/glad.h"
#include <glm/glm.hpp>

namespace k1526::engine {
namespace implementation {

class material
{
public:
	material();
	virtual ~material();

	auto set_to_program(GLuint program) -> void;

	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::vec4 emissive;
	float specular_power;
};

} // namespace implementation

// lift the implementation
using material = implementation::material;

} // namespace k1526:engine
