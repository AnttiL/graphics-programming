#pragma once

#include <fstream>
#include <optional>
#include <streambuf>
#include <string>
#include <string_view>
#include <vector>

#include "glad/glad.h"
#include <glm/glm.hpp>
#include <spdlog/spdlog.h>

namespace k1526::engine {
namespace implementation {

class shader
{
public:
        /// constructor
        ///
        shader(std::string_view vertex_shader_file_path, std::string_view fragment_shader_file_path);
        auto get_program() const -> GLuint;
        /// activate the shader
        ///
        auto activate() const -> void;
        /// set an uniform bool value
        ///
        auto set_bool(std::string_view name, bool value) const -> void;
        /// set an uniform float value
        ///
        auto set_float(std::string_view name, float value) const -> void;
        /// set an uniform int value
        ///
        auto set_int(std::string_view name, int value) const -> void;
        /// set an uniform mat4 value
        ///
        auto set_mat4(std::string_view name, const glm::mat4& value) const -> void;
        /// set an uniform vec4 value
        ///
        auto set_vec4(std::string_view name, const glm::vec4& value) const -> void;
private:
        /// create a shader
        ///
        auto create(std::string_view vertex_shader_file_path, std::string_view fragment_shader_file_path) -> void;
        /// create a fragment shader from a file
	///
	auto create_fragment_shader_from_file(std::string_view file_path) -> GLuint;
	/// create a vertex shader from a file
	///
	auto create_vertex_shader_from_file(std::string_view file_path) -> GLuint;
        /// load file into a string
	///
	auto load_file(std::string_view file_path) -> std::optional<std::string>;
        /// print program error
        ///
        auto print_program_error() -> void;
        /// print shader error
	///
	auto print_shader_error(GLuint shader) -> void;

        std::shared_ptr<spdlog::logger> logger;	///< spdlog logger
        GLuint program;
};

} // namespace implementation

// lift the implementation
using shader = implementation::shader;

} // namespace k1526::engine
