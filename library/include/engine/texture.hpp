#pragma once

#include <string_view>

#include "glad/glad.h"
#include <spdlog/spdlog.h>

namespace k1526::engine {
namespace implementation {

class texture
{
public:
        /// constructor
        ///
        texture(std::string_view texture_file_path);
        /// get texture handle
        ///
        auto get() const -> GLuint;
private:
        /// create texture
        ///
        auto create(std::string_view texture_file_path) -> void;

        std::shared_ptr<spdlog::logger> logger;	///< spdlog logger
        GLuint texture_handle;
};

} // namespace implementation

// lift the implementation
using texture = implementation::texture;

} // namespace k1526::engine
