// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include <glm/glm.hpp>

namespace k1526::engine {
namespace implementation {

struct vertex
{
	/// constructor
	///
	vertex();
	/// constructor
	///
	vertex(float pos_x, float pos_y, float pos_z, float norm_x, float norm_y, float norm_z, float tex_u, float tex_v);
	/// constructor
	///
	vertex(const glm::vec3& position, const glm::vec3& normal, float tex_u, float tex_v);
	/// get the number of bytes allocated by one vertex
	///
	static auto get_stride() -> int;

	float position_x;	///< x position
        float position_y;     	///< y position
        float position_z;     	///< z position
        float normal_x;     	///< x normal
        float normal_y;     	///< y normal
        float normal_z;     	///< z normal
	float texture_u;	///< texture u coordinate
	float texture_v;	///< texture v coordinate
};

} // namespace implementation

// lift the implementation
using vertex = implementation::vertex;

} // namespace k1526::engine
