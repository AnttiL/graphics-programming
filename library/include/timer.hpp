// Original work Jani Immonen
// Modified work Antti Lohtaja

#pragma once

#include <cstdint>

namespace k1526 {
namespace implementation {

/// create a basic high-precision timer
///
class timer
{
public:
        /// constructor
        ///
	timer();
        /// destructor
        ///
	virtual ~timer();
        /// initialize the timer
	///
	auto create() -> void;
	/// start calculating the time
	///
	auto begin_timer() -> void;
	/// top calculating the time
	///
	auto end_timer() -> void;
	/// get elapsed seconds
        ///
	/// \return seconds between last begin_timer - end_timer calls.
	auto get_elapsed_seconds() const -> float;
	/// get ticks
        ///
        /// \return system timer ticks
	static auto get_ticks() -> int64_t;
private:
	double rate_to_seconds; ///< ticks to seconds conversion
	int64_t start_clock;    ///< timer start ticks
	int64_t tick_frequency; ///< tick frequency
	float elapsed_seconds;  ///< elapsed time
};

} // namespace implementation

// lift the implementation
using timer = implementation::timer;

} // namespace k1526
