// Original work Jani Immonen
// Modified work Antti Lohtaja

#include "application.hpp"

namespace k1526::implementation {

static void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/)
{
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
                glfwSetWindowShouldClose(window, GLFW_TRUE);
}

application::application()
        : logger{}
        , window{}
        , active{false}
        , screen_width{0}
        , screen_height{0}
{ }

application::~application()
{
        glfwDestroyWindow(window);
	glfwTerminate();
}

auto application::get_aspect_ratio() const -> float
{
        return (float)screen_width / (float)screen_height;
}

auto application::get_window() -> window_ptr
{
        return window;
}

auto application::handle_resize(window_ptr /*window*/, int width, int height) -> void
{
        //glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);
}

auto application::handle_error(int /*error*/, const char* description) -> void
{
        spdlog::error("Error: {}", description);
}

auto application::create(int window_width, int window_height, std::string_view window_title) -> bool
{
        spdlog::debug("Creating application..");

        // set error handler
	glfwSetErrorCallback(handle_error);

        // initialize glfw functions
	if (not glfwInit()) {
                spdlog::error("Could not initialize glfw.");
		return false;
        }

        // create a window
        screen_width = window_width;
        screen_height = window_height;
        if (not create_window(window_width, window_height, window_title)) {
                spdlog::error("Could not create a window.");
		glfwTerminate();
                return false;
        }

        // set window resize and key event handlers
        glfwSetFramebufferSizeCallback(window, handle_resize);
	glfwSetKeyCallback(window, key_callback);

        // set the opengl context of our window as current
	glfwMakeContextCurrent(window);

        // load opengl function pointers
	if (not gladLoadGL()) {
                spdlog::error("Something went wrong.");
                return false;
        }
        spdlog::debug("Got OpenGL {}.{}", GLVersion.major, GLVersion.minor);

        // set initial viewport size
        glViewport(0, 0, window_width, window_height);

        // enable depth test
        glEnable(GL_DEPTH_TEST);

        on_create();

        spdlog::debug("Application created.");
        return true;
}


auto application::run() -> void
{
	//glfwSwapInterval(1);

	while (!glfwWindowShouldClose(window))
	{
                on_draw();
		on_update();

	        glfwSwapBuffers(window);
		glfwPollEvents();
	}
}

auto application::on_create() -> void
{ }

auto application::on_draw() -> void
{ }

auto application::on_update() -> void
{ }

auto application::create_window(int window_width, int window_height, std::string_view window_title) -> bool
{
        // set some hints about the window
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        //glfwWindowHint(GLFW_DEPTH_BITS, 16);
        //glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE);

        // try to create a window
        window = glfwCreateWindow(window_width, window_height, window_title.data(), NULL, NULL);
        if (not window)
                return false;

        return true;
}

} // namespace k1526::implementation
