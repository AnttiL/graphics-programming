// Original work Jani Immonen
// Modified work Antti

#include "engine/game_object.hpp"

namespace k1526::engine::implementation {

game_object::game_object()
        : model{1.0f}
	, view{nullptr}
	, projection{nullptr}
        , geometry{nullptr}
        , material{nullptr}
	, shader{nullptr}
        , rotation_angle{0.0f}
        , rotation_speed{0.0f}
        , gravity{0.0f}
        , velocity{0.0f}
{
	//model[3][0] = glm::linearRand(-10.0f, 10.0f);
	//model[3][1] = glm::linearRand(-10.0f, 10.0f);
	//model[3][2] = glm::linearRand(-10.0f, 10.0f);
}

game_object::~game_object()
{ }

auto game_object::attach_shader(shader_ptr shader_) -> void
{
	shader = shader_;
}

auto game_object::get_gravity() const -> float
{
        return gravity;
}

auto game_object::get_matrix() -> glm::mat4&
{
        return model;
}

auto game_object::get_material() -> material_ptr
{
        return material;
}

auto game_object::get_position() const -> glm::vec3
{
        return model[3];
}

auto game_object::get_radius() const -> float
{
        return 2.0f;
}

auto game_object::get_rotation_angle() const -> float
{
        return rotation_angle;
}

auto game_object::get_rotation_speed() const -> float
{
        return rotation_speed;
}

auto game_object::get_velocity() -> glm::vec3&
{
        return velocity;
}

auto game_object::get_geometry() const -> geometry_ptr
{
        return geometry;
}

auto game_object::set_geometry(geometry_ptr geometry_) -> void
{
        geometry = geometry_;
}

auto game_object::set_gravity(float gravity_) -> void
{
        gravity = gravity_;
}

auto game_object::set_material(material_ptr material_) -> void
{
        material = material_;
}

auto game_object::set_position(const glm::vec3& position) -> void
{
        model[3][0] = position.x;
        model[3][1] = position.y;
        model[3][2] = position.z;
}

auto game_object::set_position(float x, float y, float z) -> void
{
        model[3][0] = x;
        model[3][1] = y;
        model[3][2] = z;
}

auto game_object::set_projection(mat4_ptr projection_) -> void
{
        projection = projection_;
}

auto game_object::set_view(mat4_ptr view_) -> void
{
	view = view_;
}

auto game_object::set_rotation_angle(float angle) -> void
{
        rotation_angle = angle;
}

auto game_object::set_rotation_speed(float speed) -> void
{
        rotation_speed = speed;
}

auto game_object::set_velocity(const glm::vec3& velocity_) -> void
{
        velocity = velocity_;
}

auto game_object::draw() -> void
{
        if (not geometry)
                return;

	if (not shader)
		return;

	auto program = shader->get_program();
	shader->activate();

        geometry->set_attributes(program);

	auto model_location = glGetUniformLocation(program, "model");
        glUniformMatrix4fv(model_location, 1, GL_FALSE, &model[0][0]);

	auto view_location = glGetUniformLocation(program, "view");
        glUniformMatrix4fv(view_location, 1, GL_FALSE, &(*view)[0][0]);

	auto projection_location = glGetUniformLocation(program, "projection");
        glUniformMatrix4fv(projection_location, 1, GL_FALSE, &(*projection)[0][0]);

        geometry->draw();

	geometry->disable_attributes(program);
}

auto game_object::update() -> void
{
	// the main loop
	auto frametime = (float)glfwGetTime();
	glfwSetTime(0);

	rotation_angle += rotation_speed * frametime;
	while (rotation_angle > glm::two_pi<float>()) {
		rotation_angle -= glm::two_pi<float>();
	}
	while (rotation_angle < 0.0f) {
		rotation_angle += glm::two_pi<float>();
	}

	// add velocity to position
	auto position = get_position();
	position += velocity * frametime;

	// add gravity to velocity
	velocity.y -= frametime * gravity;

	auto rotation_model = glm::mat4{1.0f};
	model = glm::rotate(rotation_model, rotation_angle, glm::normalize(glm::vec3(0.4f, 0.5f, 0.0f)));

	model[3][0] = position.x;
	model[3][1] = position.y;
	model[3][2] = position.z;
}

} // namespace k1526::engine::implementation
