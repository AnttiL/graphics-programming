// Original work Jani Immonen
// Modified work Antti Lohtaja

#include "engine/geometry.hpp"

namespace k1526::engine::implementation {

auto vertex_buffer_object = uint32_t{0};
auto vertex_array_object = uint32_t{0};
auto element_buffer_object = uint32_t{0};

geometry::geometry()
        : vertices{}
	, index_count{0}
{ }

geometry::~geometry()
{
	//clear();
}

auto geometry::clear() -> void
{
	vertices.clear();
	if (vertex_buffer_object)
	{
		glDeleteBuffers(1, &vertex_buffer_object);
		vertex_buffer_object = 0;
	}
	index_count = 0;
}

auto geometry::generate_cube(const glm::vec3& size, const glm::vec3& offset) -> void
{
	//clear();
	generate_cube(size, offset, vertices, index_count);
}

auto geometry::generate_plane(const glm::vec3& size, const glm::vec3& offset) -> void
{
	//clear();
	generate_plane(size, offset, vertices, index_count);
}

auto geometry::generate_sphere(const glm::vec3& radius, const glm::vec3& offset, uint32_t rings, uint32_t segments) -> void
{
	//clear();
	generate_sphere(radius, offset, rings, segments, vertices);
}

auto geometry::set_attributes(GLuint program) const -> void
{
	// position
	auto position = glGetAttribLocation(program, "our_position");
	glEnableVertexAttribArray(position);
	glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, vertex::get_stride(), (void*)0);

	// normal
	auto normal = glGetAttribLocation(program, "our_normal");
	glEnableVertexAttribArray(normal);
	glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, vertex::get_stride(), (void*)(3 * sizeof(float)));

	// texture coordinates
	auto texture_coordinates = glGetAttribLocation(program, "our_texture_coordinates");
	glEnableVertexAttribArray(texture_coordinates);
	glVertexAttribPointer(texture_coordinates, 2, GL_FLOAT, GL_FALSE, vertex::get_stride(), (void*)(6 * sizeof(float)));
}

auto geometry::disable_attributes(GLuint program) const -> void
{
	auto position = glGetAttribLocation(program, "our_position");
	auto normal = glGetAttribLocation(program, "our_normal");
	auto texture_coordinates = glGetAttribLocation(program, "our_texture_coordinates");

	glDisableVertexAttribArray(position);
	glDisableVertexAttribArray(normal);
	glDisableVertexAttribArray(texture_coordinates);
}

auto geometry::draw() const -> void
{
	if (not vertex_array_object)
		return;

	if (index_count) {
		glBindVertexArray(vertex_array_object); // VAO
        	glDrawElements(GL_TRIANGLES, index_count, GL_UNSIGNED_INT, 0);
	} else {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)get_vertex_count());
	}
}


auto geometry::generate_cube(const glm::vec3& size, const glm::vec3& offset, std::vector<vertex>& vertices, size_t& index_count) -> void
{
	// cube sizes
	const auto cube_width = float{size.x * 0.5f};
	const auto cube_height = float{size.y * 0.5f};
	const auto cube_depth = float{size.z * 0.5f};

	// normals
	const auto normal1 = glm::vec3{1.0f, 0.0f, 0.0f};
	const auto normal2 = glm::vec3{0.0f, 1.0f, 0.0f};
	const auto normal3 = glm::vec3{0.0f, 0.0f, 1.0f};

	// cube corners
	const auto p1 = glm::vec3{-cube_width + offset.x,  cube_height + offset.y,  cube_depth + offset.z};
	const auto p2 = glm::vec3{ cube_width + offset.x,  cube_height + offset.y,  cube_depth + offset.z};
	const auto p3 = glm::vec3{ cube_width + offset.x, -cube_height + offset.y,  cube_depth + offset.z};
	const auto p4 = glm::vec3{-cube_width + offset.x, -cube_height + offset.y,  cube_depth + offset.z};
	const auto p5 = glm::vec3{-cube_width + offset.x, -cube_height + offset.y, -cube_depth + offset.z};
	const auto p6 = glm::vec3{ cube_width + offset.x, -cube_height + offset.y, -cube_depth + offset.z};
	const auto p7 = glm::vec3{ cube_width + offset.x,  cube_height + offset.y, -cube_depth + offset.z};
	const auto p8 = glm::vec3{-cube_width + offset.x,  cube_height + offset.y, -cube_depth + offset.z};

	// add vertices
	using vertex = k1526::engine::vertex;
	// front face
	vertices.push_back(vertex{p1, normal3, 0.0f, 0.0f});
	vertices.push_back(vertex{p2, normal3, 1.0f, 0.0f});
	vertices.push_back(vertex{p3, normal3, 1.0f, 1.0f});
	vertices.push_back(vertex{p4, normal3, 0.0f, 1.0f});
	// right face
	vertices.push_back(vertex{p2, normal1, 0.0f, 0.0f});
	vertices.push_back(vertex{p7, normal1, 1.0f, 0.0f});
	vertices.push_back(vertex{p6, normal1, 1.0f, 1.0f});
	vertices.push_back(vertex{p3, normal1, 0.0f, 1.0f});
	// back face
	vertices.push_back(vertex{p7, normal3, 0.0f, 0.0f});
	vertices.push_back(vertex{p8, normal3, 1.0f, 0.0f});
	vertices.push_back(vertex{p5, normal3, 1.0f, 1.0f});
	vertices.push_back(vertex{p6, normal3, 0.0f, 1.0f});
	// left face
	vertices.push_back(vertex{p8, normal1, 0.0f, 0.0f});
	vertices.push_back(vertex{p1, normal1, 1.0f, 0.0f});
	vertices.push_back(vertex{p4, normal1, 1.0f, 1.0f});
	vertices.push_back(vertex{p5, normal1, 0.0f, 1.0f});
	// top face
	vertices.push_back(vertex{p8, normal2, 0.0f, 0.0f});
	vertices.push_back(vertex{p7, normal2, 1.0f, 0.0f});
	vertices.push_back(vertex{p2, normal2, 1.0f, 1.0f});
	vertices.push_back(vertex{p1, normal2, 0.0f, 1.0f});
	// bottom face
	vertices.push_back(vertex{p4, normal2, 0.0f, 0.0f});
	vertices.push_back(vertex{p3, normal2, 1.0f, 0.0f});
	vertices.push_back(vertex{p6, normal2, 1.0f, 1.0f});
	vertices.push_back(vertex{p5, normal2, 0.0f, 1.0f});

	// create indices to cube object
	auto indices = std::array<uint32_t, 36>{};

	int32_t i, j;
	for (i = 0, j = 0; i < 21; i += 4, j += 6)
	{
		indices[j] = (i + 2);
		indices[j + 1] = (i + 1);
		indices[j + 2] = (i + 0);
		indices[j + 3] = (i + 0);
		indices[j + 4] = (i + 3);
		indices[j + 5] = (i + 2);
	}

	glGenVertexArrays(1, &vertex_array_object);
	glGenBuffers(1, &vertex_buffer_object);
	glGenBuffers(1, &element_buffer_object);

	glBindVertexArray(vertex_array_object);

	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * vertex::get_stride(), vertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32_t), indices.data(), GL_STATIC_DRAW);

	index_count = 36;
}

auto geometry::generate_plane(const glm::vec3& size, const glm::vec3& offset, std::vector<vertex>& vertices, size_t& index_count) -> void
{
	// sizes
	const auto plane_width = float{size.x * 0.5f};
	const auto plane_height = float{size.y * 0.5f};
	const auto plane_depth = float{0.0f};

	// normal
	const auto normal = glm::vec3{0.0f, 0.0f, 0.0f};

	// corners
	const auto top_right 	= glm::vec3{ plane_width + offset.x,  plane_height + offset.y, plane_depth};
	const auto bottom_right = glm::vec3{ plane_width + offset.x, -plane_height + offset.y, plane_depth};
	const auto bottom_left	= glm::vec3{-plane_width + offset.x, -plane_height + offset.y, plane_depth};
	const auto top_left 	= glm::vec3{-plane_width + offset.x,  plane_height + offset.y, plane_depth};

	// add vertices
	using vertex = k1526::engine::vertex;
	vertices.push_back(vertex{top_right,    normal, 1.0f, 1.0f});
	vertices.push_back(vertex{bottom_right, normal, 1.0f, 0.0f});
	vertices.push_back(vertex{bottom_left,  normal,	0.0f, 0.0f});
	vertices.push_back(vertex{top_left,     normal,	0.0f, 1.0f});

	// create indices to cube object
	auto indices = std::array<uint32_t, 6>{
		0, 1, 3,
		1, 2, 3
	};

	glGenVertexArrays(1, &vertex_array_object);
	glGenBuffers(1, &vertex_buffer_object);
	glGenBuffers(1, &element_buffer_object);

	glBindVertexArray(vertex_array_object);

	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * vertex::get_stride(), vertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32_t), indices.data(), GL_STATIC_DRAW);


	index_count = 6;
}

auto geometry::generate_sphere(const glm::vec3& radius, const glm::vec3& offset, uint32_t rings, uint32_t segments, std::vector<vertex>& vertices_) -> void
{
	float fDeltaRingAngle = (glm::pi<float>() / rings);
	float fDeltaSegAngle = (glm::two_pi<float>() / segments);

	uint32_t ring;
	for (ring = 0; ring < rings; ring++)
	{
		const float r0 = sinf((ring + 0) * fDeltaRingAngle);
		const float r1 = sinf((ring + 1) * fDeltaRingAngle);
		const float y0 = cosf((ring + 0) * fDeltaRingAngle);
		const float y1 = cosf((ring + 1) * fDeltaRingAngle);

		uint32_t seg;
		for (seg = 0; seg < (segments + 1); seg++)
		{
			const float x0 = r0 * sinf(seg * fDeltaSegAngle);
			const float z0 = r0 * cosf(seg * fDeltaSegAngle);
			const float x1 = r1 * sinf(seg * fDeltaSegAngle);
			const float z1 = r1 * cosf(seg * fDeltaSegAngle);

			vertices_.push_back(vertex(
				radius.x * x0, radius.y * y0, radius.z * z0,
				x0, y0, z0,
				((float)seg) / segments,
				(ring / (float)rings)));

			vertices_.push_back(vertex(
				radius.x * x1, radius.y * y1, radius.z * z1,
				x1, y1, z1,
				((float)seg) / segments,
				(ring + 1) / (float)rings));
		}
	}
}


auto geometry::get_data() -> vertex*
{
        return vertices.data();
}

auto geometry::get_data() const -> const vertex*
{
        return vertices.data();
}

auto geometry::get_vertex_count() const -> size_t
{
        return vertices.size();
}

auto geometry::get_index_count() const -> size_t
{
        return index_count;
}

} // namespace k1526::engine::implementation
