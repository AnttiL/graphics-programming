// Original work Jani Immonen
// Modified work Antti Lohtaja

#include "engine/material.hpp"

namespace k1526::engine::implementation {

material::material()
        : ambient{glm::vec4(0.2f, 0.2f, 0.2f, 0.0f)}
	, diffuse{glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)}
	, specular{glm::vec4(1.0f, 1.0f, 1.0f, 0.0f)}
	, emissive{glm::vec4(0.0f, 0.0f, 0.0f, 0.0f)}
	, specular_power{50.0f}
{ }

material::~material()
{ }

auto material::set_to_program(GLuint program) -> void
{
	auto location = glGetUniformLocation(program, "materialAmbient");
	if (location != -1)
		glUniform4f(location, ambient.r, ambient.g, ambient.b, ambient.a);

	location = glGetUniformLocation(program, "materialDiffuse");
	if (location != -1)
		glUniform4f(location, diffuse.r, diffuse.g, diffuse.b, diffuse.a);

	location = glGetUniformLocation(program, "materialSpecular");
	if (location != -1)
		glUniform4f(location, specular.r, specular.g, specular.b, specular.a);

	location = glGetUniformLocation(program, "materialEmissive");
	if (location != -1)
		glUniform4f(location, emissive.r, emissive.g, emissive.b, emissive.a);

	location = glGetUniformLocation(program, "specularPower");
	if (location != -1)
		glUniform1f(location, specular_power);
}

} // namespace k1526:engine::implementation
