#include "engine/shader.hpp"

namespace k1526::engine::implementation {

shader::shader(std::string_view vertex_shader_file_path, std::string_view fragment_shader_file_path)
{
        logger = spdlog::get("Shader");
        if (not logger)
                logger = spdlog::default_logger()->clone("Shader");

        create(vertex_shader_file_path, fragment_shader_file_path);
}

auto shader::get_program() const -> GLuint
{
        return program;
}

auto shader::activate() const -> void
{
        glUseProgram(program);
}

auto shader::set_bool(std::string_view name, bool value) const -> void
{
        glUniform1i(glGetUniformLocation(program, name.data()), value);
}

auto shader::set_float(std::string_view name, float value) const -> void
{
        glUniform1i(glGetUniformLocation(program, name.data()), value);
}

auto shader::set_mat4(std::string_view name, const glm::mat4& value) const -> void
{
        glUniformMatrix4fv(glGetUniformLocation(program, name.data()), 1, GL_FALSE, &value[0][0]);
}

auto shader::set_int(std::string_view name, int value) const -> void
{
        glUniform1i(glGetUniformLocation(program, name.data()), value);
}

auto shader::set_vec4(std::string_view name, const glm::vec4& value) const -> void
{
        glUniform4fv(glGetUniformLocation(program, name.data()), 1, &value[0]);
}

auto shader::create(std::string_view vertex_shader_file_path, std::string_view fragment_shader_file_path) -> void
{
        // load and compile shaders
        auto vertex_shader = create_vertex_shader_from_file(vertex_shader_file_path);
        auto fragment_shader = create_fragment_shader_from_file(fragment_shader_file_path);

        if (not vertex_shader or not fragment_shader)
                return;

        // link shaders to our program
        program = glCreateProgram();
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);

        auto success = int{0};
        glLinkProgram(program);
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        if (not success) {
                print_program_error();
        }

        // once linked, the shaders are no longer needed
        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);
}

auto shader::create_fragment_shader_from_file(std::string_view file_path) -> GLuint
{
        logger->debug("Creating a fragment shader from file {} ..", file_path);

        auto shader_data = load_file(file_path);                // load shader source code
        if (not shader_data) {
                logger->error("Failed to create a fragment shader from file {}.", file_path);
                return 0;
        }

        auto fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);  // create shader object

        // load the source code into the shader object
	glShaderSource(fragment_shader, 1, (const char**)&shader_data.value(), nullptr);

        // compile the shader
        logger->debug("Compiling fragment shader ..");
	glCompileShader(fragment_shader);

        // check if the compilation succeeded
        auto success = GLint{false};
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
        if (not success) {
                print_shader_error(fragment_shader);
                return 0;
        }

        logger->debug("Fragment shader created.");

        return fragment_shader;
}

auto shader::create_vertex_shader_from_file(std::string_view file_path) -> GLuint
{
        logger->debug("Creating a vertex shader from file {} ..", file_path);

        auto shader_data = load_file(file_path);                // load shader source code
        if (not shader_data) {
                logger->error("Failed to create a vertex shader from file {}.", file_path);
                return 0;
        }

        auto vertex_shader = glCreateShader(GL_VERTEX_SHADER);  // create shader object

        // load the source code into the shader object
	glShaderSource(vertex_shader, 1, (const char**)&shader_data.value(), nullptr);

        // compile the shader
        logger->debug("Compiling vertex shader ..");
	glCompileShader(vertex_shader);

        // check if the compilation succeeded
        auto success = GLint{false};
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
        if (not success) {
                print_shader_error(vertex_shader);
                return 0;
        }

        logger->debug("Vertex shader created.");

        return vertex_shader;
}

auto shader::load_file(std::string_view file_path) -> std::optional<std::string>
{
        auto file_string = std::string{};
        auto file_stream = std::ifstream{file_path.data()};

        if (not file_stream) {
                logger->error("Failed to open file: {}", file_path);
                return std::nullopt;
        }

        file_stream.seekg(0, std::ios::end);
        file_string.reserve(file_stream.tellg());
        file_stream.seekg(0, std::ios::beg);

        file_string.assign(
                std::istreambuf_iterator<char>(file_stream),
                std::istreambuf_iterator<char>()
        );

        return file_string;
}

auto shader::print_program_error() -> void
{
        auto error_length = GLsizei{0};
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &error_length);

        if (not error_length) {
                logger->error("Couldn't print program error.");
                return;
        }

        auto characters_written = GLsizei{0};
        auto message = std::vector<GLchar>{};
        message.reserve(error_length);

	glGetProgramInfoLog(program, error_length, &characters_written, message.data());
        logger->error("Failed to link program: {}", message.data());
}

auto shader::print_shader_error(GLuint shader) -> void
{
        auto error_length = GLsizei{0};
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &error_length);

        if (not error_length) {
                logger->error("Couldn't print shader error.");
                return;
        }

        auto characters_written = GLsizei{0};
        auto message = std::vector<GLchar>{};
        message.reserve(error_length);

        glGetShaderInfoLog(shader, error_length, &characters_written, message.data());

        logger->error("Failed to compile shader: {}", message.data());
}

} // namespace k1526::engine::implementation
