#include "engine/texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace k1526::engine::implementation {

texture::texture(std::string_view texture_file_path)
        : texture_handle{0}
{
        logger = spdlog::get("Texture");
        if (not logger)
                logger = spdlog::default_logger()->clone("Texture");

        create(texture_file_path);
}

auto texture::get() const -> GLuint
{
        return texture_handle;
}

auto texture::create(std::string_view texture_file_path) -> void
{
        logger->debug("Creating a texture from file {} ..", texture_file_path);

        auto texture_width = int{0};
        auto texture_height = int{0};
        auto color_channel_count = int{0};

        stbi_set_flip_vertically_on_load(true);
	auto image_data = stbi_load(texture_file_path.data(), &texture_width, &texture_height, &color_channel_count, STBI_rgb_alpha);
	if (not image_data or not texture_width or not texture_height or not color_channel_count) {
                logger->error("Failed to load texture file: {}", texture_file_path);
                return;
        }

	glGenTextures(1, &texture_handle);
	glBindTexture(GL_TEXTURE_2D, texture_handle);

        constexpr auto internal_format = GLint{GL_RGBA};
        constexpr auto format = GLenum{GL_RGBA};
        glTexImage2D(GL_TEXTURE_2D, 0, internal_format, texture_width, texture_height, 0, format, GL_UNSIGNED_BYTE, image_data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        stbi_image_free(image_data);

        logger->debug("Texture created.");
}

} // namespace k1526::engine::implementation
