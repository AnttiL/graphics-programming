// Original work Jani Immonen
// Modified work Antti Lohtaja

#include "engine/vertex.hpp"

namespace k1526::engine::implementation {

vertex::vertex()
        : position_x{0.0f}
        , position_y{0.0f}
        , position_z{0.0f}
        , normal_x{0.0f}
        , normal_y{0.0f}
        , normal_z{0.0f}
        , texture_u{0.0f}
        , texture_v{0.0f}
{ }

vertex::vertex(float pos_x, float pos_y, float pos_z, float norm_x, float norm_y, float norm_z, float tex_u, float tex_v)
        : position_x{pos_x}
        , position_y{pos_y}
        , position_z{pos_z}
        , normal_x{norm_x}
        , normal_y{norm_y}
        , normal_z{norm_z}
        , texture_u{tex_u}
        , texture_v{tex_v}
{ }

vertex::vertex(const glm::vec3& position, const glm::vec3& normal, float tex_u, float tex_v)
        : position_x{position.x}
        , position_y{position.y}
        , position_z{position.z}
        , normal_x{normal.x}
        , normal_y{normal.y}
        , normal_z{normal.z}
        , texture_u{tex_u}
        , texture_v{tex_v}
{ }

auto vertex::get_stride() -> int
{
        return 32;
}

} // namespace k1526::engine::implementation
