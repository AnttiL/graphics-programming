// Original work Jani Immonen
// Modified work Antti Lohtaja

#include "timer.hpp"

namespace k1526::implementation {

#include <time.h>
#include <sys/time.h>

timer::timer()
        : rate_to_seconds{0.0f}
	, start_clock{0}
	, tick_frequency{0}
	, elapsed_seconds{0.0f}
{ }

timer::~timer()
{ }

auto timer::create() -> void
{
        rate_to_seconds = 1.0 / 1000000000.0;
	tick_frequency = 10000000;
}

auto timer::begin_timer() -> void
{
	start_clock = get_ticks();
}

auto timer::end_timer() -> void
{
	auto const end_clock = get_ticks();
	if (end_clock == start_clock || start_clock == 0) {
		elapsed_seconds = 0.000001f;
	} else {
		double ticks = (double)(end_clock - start_clock);
		elapsed_seconds = (float)(ticks * rate_to_seconds);
	}
}

auto timer::get_elapsed_seconds() const -> float
{
	return elapsed_seconds;
}

auto timer::get_ticks() -> int64_t
{
	struct timespec time;
	clock_gettime(CLOCK_MONOTONIC_RAW, &time);
	static long long timevalsub = 0;
	if (timevalsub == 0)
		timevalsub = (long long)time.tv_sec;
	time.tv_sec -= timevalsub;
	return (int64_t)(time.tv_sec * 1000000000llu + time.tv_nsec);
}

} // namespace k1526::implementation
